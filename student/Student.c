#include "../tools/student.h"
#include<stdio.h>
#include<string.h>
#include<malloc.h>
#include"Student_Interface.h"

//学生信息节点构建函数
static Student* create_student(int student_id, const char *name) {
    Student *student = (Student *)malloc(sizeof(Student));
    if (student) {
        student->student_id = student_id;
        strcpy(student->name, name);
        student->selected_courses = NULL;
        student->total_credits = 0.0;
        student->next = NULL;
    }
    return student;
}
//增添节点函数
void add_student(Student **head, int student_id, const char *name){
	Student *newstudent=create_student(student_id,name);
	if(!newstudent){
		printf("新建学生信息节点失败\n");
		return;
	}
	newstudent->next=*head;
	*head=newstudent;
}
//修改学生信息
void modify_student(Student *head, int student_id, const char *name){
	Student *current=head;
	while(current){
		if(current->student_id==student_id){
			strcpy(current->name,name);
			printf("学生信息修改成功\n");
			return;
		}
		current=current->next;
	}
	printf("未查找到该学生\n");
}
//删除学生信息
void delete_student(Student **head, int student_id){
	Student *current=*head;
	Student *prev=NULL;
	while(current){
		if(current->student_id==student_id){
			if(prev){
				prev->next=current->next;
			}else{
				*head=current->next;
			}
			free(current);
			printf("成功删除学号为：%d的学生信息\n",student_id);
			return;
		}
		prev=current;
		current=current->next;
	}
	printf("未查找到学号：%d的学生信息\n",student_id);
}
//根据学号打印信息
void view_student_byid(Student *head, int student_id){
	Student *current = head;
    while (current) {
        if (current->student_id == student_id) {
            printf("学生ID: %d, 姓名: %s, 总学分: %d\n", current->student_id, current->name,current->total_credits);
            // 打印所选课程列表
            printf("选课列表为:\n");
            Course *selected_course = current->selected_courses;
            while (selected_course) {
                printf("  课程ID: %d, 课程名称为: %s\n", selected_course->course_id, selected_course->name);
                selected_course = selected_course->next;
            }
            return; // 找到匹配的学生，退出函数
        }
        current = current->next;
    }
    printf("没有找到学号为：%d的学生.\n", student_id);
}
//根据名称打印信息
void view_student_byname(Student *head, const char *name){
	Student *current = head;
    while (current) {
        if (strcmp(current->name, name) == 0) {
            printf("学生 ID: %d, 姓名: %s, 学分: %d\n", current->student_id, current->name,current->total_credits);
            // 打印所选课程列表
            printf("选课列表为:\n");
            Course *selected_course = current->selected_courses;
            while (selected_course) {
                printf("  课程ID: %d, 课程名称为: %s\n", selected_course->course_id, selected_course->name);
                selected_course = selected_course->next;
            }
            return; // 找到匹配的学生，退出函数
        }
        current = current->next;
    }
    printf("没有找到姓名为：%s的学生.\n", name);
}
//添加课程列表
void enroll_student(Student *student, Course *course) {
    //判断课程是否已经添加
	Course *current_course = student->selected_courses;
    while (current_course) {
        if (current_course == course) {
            printf("Student is already enrolled in this course.\n");
            return;
        }
        current_course = current_course->next;
    }
    // Enroll the student in the course
    course->current_students++;
    student->total_credits += course->credits;
    student->selected_courses = course;
    printf("Student enrolled in course: %s\n", course->name);
}
//将学生从课程中移除
void withdraw_student(Student *student, Course *course) {
    Course *current_course = student->selected_courses;
    Course *prev_course = NULL;
    while (current_course) {
        if (current_course == course) {
            // 更新总学分
            student->total_credits -= course->credits;
            // 更新课程人数
            course->current_students--;
            // 从学生选课列表中移除
            if (prev_course) {
                prev_course->next = current_course->next;
            } else {
                student->selected_courses = current_course->next;
            }
            printf("Student withdrawn from course: %s\n", course->name);
            return;
        }
        prev_course = current_course;
        current_course = current_course->next;
    }
    printf("Student is not enrolled in this course.\n");
}
