#ifndef STUDENT_MANAGE_H
#define STUDENT_MANAGE_H

#include"../tools/student.h"
void add_student(Student **head, int student_id, const char *name);//增添学生链表
void modify_student(Student *head, int student_id, const char *name);//修改学生信息
void delete_student(Student **head, int student_id);//删除学生信息
void view_student_byid(Student *head, int student_id);//通过学号查看学生信息
void view_student_byname(Student *head, const char *name);//通过名称查看学生信息
void enroll_student(Student *student, Course *course);
void withdraw_student(Student *student, Course *course);

#endif // STUDENT_MANAGE_H
