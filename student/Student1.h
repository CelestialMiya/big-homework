#ifndef STUDENT1_H
#define STUDENT1_H

#include "../tools/student.h"
#include "../tools/database.h"
//数据库初始化
void initializeDatabase();
//关闭数据库
void closeDatabase();
//插入课程信息
bool insertCourse(const Course *course);
//删除课程信息
bool deleteCourse(const Course *course);
//查找课程信息
Course* readCourse(const Course *course);
//更新课程信息
bool updateCourse(const Course *course);
//添加学生信息
bool insertStudent(const Student *student);
//更新学生信息
bool updateStudent(const Student *student);
//删除学生信息
bool deleteStudent(const Student *student);
//查找学生信息
Student* findStudent(const Student *student);
#endif


