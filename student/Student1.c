#include <mysql/mysql.h>
#include "Student1.h" 
#include<stdio.h>
#include<string.h>
#include "../tools/cJSON.h"// 包含 cJSON 库头文件
#include "../tools/database.h"
// 定义函数来解析 JSON 数据并返回课程结构体
Course* parseCourseJSON(cJSON *json) {
    Course *course = (Course *)malloc(sizeof(Course));
    if (course == NULL) {
        fprintf(stderr, "Memory allocation failed.\n");
        return NULL;
    }

    cJSON *courseIdJson = cJSON_GetObjectItem(json, "course_id");
    cJSON *courseNameJson = cJSON_GetObjectItem(json, "course_name");
    cJSON *courseNatureJson = cJSON_GetObjectItem(json, "course_nature");
    cJSON *courseHoursJson = cJSON_GetObjectItem(json, "course_hours");
    cJSON *courseCreditsJson = cJSON_GetObjectItem(json, "course_credits");

    if (cJSON_IsNumber(courseIdJson) &&
        cJSON_IsString(courseNameJson) &&
        cJSON_IsString(courseNatureJson) &&
        cJSON_IsNumber(courseHoursJson) &&
        cJSON_IsNumber(courseCreditsJson)) {
        course->course_id = courseIdJson->valueint;
        strcpy(course->name, courseNameJson->valuestring);
        strcpy(course->nature, courseNatureJson->valuestring);
        course->hours = courseHoursJson->valueint;
        course->credits = courseCreditsJson->valueint;
        course->next = NULL;
        return course;
    } else {
        free(course);
        fprintf(stderr, "Invalid course JSON.\n");
        return NULL;
    }
}
// 添加学生信息
bool insertStudent(const Student *student) {
    initializeDatabase();
	// 实现插入学生信息的代码
	   // 创建SQL语句
	 // 创建课程数e
    cJSON *coursesArray = cJSON_CreateArray();
    // 遍历学生的课程链表并将每个课程添加到数组中
    Course *currentCourse = student->selected_courses;
    while (currentCourse != NULL) {
        cJSON *courseObject = cJSON_CreateObject(); // 创建课程对象
        cJSON_AddNumberToObject(courseObject, "course_id", currentCourse->course_id);
        cJSON_AddStringToObject(courseObject, "course_name", currentCourse->name);
        cJSON_AddNumberToObject(courseObject, "course_hours", currentCourse->hours);
        cJSON_AddNumberToObject(courseObject, "course_credits", currentCourse->credits);
        cJSON_AddItemToArray(coursesArray, courseObject); // 将课程对象添加到数组
        currentCourse = currentCourse->next;
    }
	// 将 JSON 对象转换为 JSON 字符串
    char *jsonStr = cJSON_Print(coursesArray);
	char query[1024];
    sprintf(query, "INSERT INTO student (student_id, name, course_list, total_credit) VALUES ('%d', '%s', '%s', %d)",
            student->student_id, student->name,jsonStr, student->total_credits);
    // 执行SQL语句
    if (mysql_query(conn, query) != 0) {
        fprintf(stderr, "Insert into student table failed: %s\n", mysql_error(conn));
        mysql_close(conn);
        return false;
    }
	closeDatabase();
	return true;
}
// 更新学生信息
bool updateStudent(const Student *student) {
    initializeDatabase();
	// 实现更新学生信息的代码
	// 创建课程数e
    cJSON *coursesArray = cJSON_CreateArray();
    // 遍历学生的课程链表并将每个课程添加到数组中
    Course *currentCourse = student->selected_courses;
    while (currentCourse != NULL) {
        cJSON *courseObject = cJSON_CreateObject(); // 创建课程对象
        cJSON_AddNumberToObject(courseObject, "course_id", currentCourse->course_id);
        cJSON_AddStringToObject(courseObject, "course_name", currentCourse->name);
        cJSON_AddNumberToObject(courseObject, "course_hours", currentCourse->hours);
        cJSON_AddNumberToObject(courseObject, "course_credits", currentCourse->credits);
        cJSON_AddItemToArray(coursesArray, courseObject); // 将课程对象添加到数组
        currentCourse = currentCourse->next;
    }
	// 将 JSON 对象转换为 JSON 字符串
    char *jsonStr = cJSON_Print(coursesArray);
    char sql[1000];
    snprintf(sql, sizeof(sql), "UPDATE Students SET name = '%s', course_list = %s WHERE student_id = %d;",
             student->name,jsonStr, student->student_id);
    if (mysql_query(conn, sql) != 0) {
        fprintf(stderr, "mysql_query() failed: %s\n", mysql_error(conn));
        closeDatabase();
		return false;
    } else {
        printf("Student updated successfully.\n");
        closeDatabase();
		return true;
    }
}
// 删除学生信息
bool deleteStudent(const Student *student) {
    // 实现删除学生信息的代码
 	initializeDatabase();
	Course *course=student->selected_courses;
	while(course){
		course->current_students--;
		Student* head=course->enrolled_students;
		Student* current=head;
		Student* pre=NULL;
		while(current){
			if(current->student_id==student->student_id){
				if(pre){
					pre->next=current->next;
				}else{
					head=current->next;
				}
				free(current);
				updateCourse(course);
			}
			pre=current;
			current=current->next;
		}
		course=course->next;
	}
	char sql[1000];
	snprintf(sql, sizeof(sql), "DELETE FROM Students WHERE student_id = %d;", student->student_id);
    if (mysql_query(conn, sql) != 0) {
        fprintf(stderr, "mysql_query() failed: %s\n", mysql_error(conn));
        closeDatabase();
		return false;
    } else {
        printf("Student deleted successfully.\n");
        closeDatabase();
		return true;
    }
}
// 查找学生信息
Student* findStudent(const Student *student) {
 initializeDatabase();
	char query[1024];
    sprintf(query, "SELECT * FROM students WHERE student_id=%d",student->student_id);
    if (mysql_query(conn, query) != 0) {
        fprintf(stderr, "Query student information failed: %s\n", mysql_error(conn));
        mysql_close(conn);
        return NULL;
	}
	 MYSQL_RES *result = mysql_store_result(conn);
    if (result == NULL) {
        fprintf(stderr, "Failed to fetch student information: %s\n", mysql_error(conn));
        mysql_close(conn);
        return NULL;
    }
 	MYSQL_ROW row = mysql_fetch_row(result);
	if (row == NULL) {
   	    fprintf(stderr, "Student not found.\n");
   	    mysql_free_result(result);
        mysql_close(conn);
        return NULL;
	}
	// 创建并填充学生结构体
	Student *student1 = (Student *)malloc(sizeof(Student));
	if (student1 == NULL) {
        fprintf(stderr, "Memory allocation failed.\n");
        mysql_free_result(result);
        mysql_close(conn);
        return NULL;
	}	
	student1->student_id = atoi(row[0]);
	strcpy(student1->name, row[1]);
	// 继续添加其他字段的赋值
	student1->total_credits = atoi(row[3]); // 假设总学分存储在第三列
	//将第三列的json转换为链表
	const char *json_str = row[2];
	cJSON *root = cJSON_Parse(json_str);
    	if (root == NULL) {
        	fprintf(stderr, "JSON parsing failed.\n");
        	return NULL;
    	}
    	// 创建课程链表头
    	Course *head = NULL;
    	// 获取 JSON 数组
    	cJSON *coursesArray = cJSON_GetObjectItem(root, "course_list");
    	if (coursesArray != NULL && cJSON_IsArray(coursesArray)) {
        	// 遍历 JSON 数组
        	cJSON *courseJson = coursesArray->child;
       		while (courseJson != NULL) {
            	// 解析 JSON 元素并将课程添加到链表
            	Course *course = parseCourseJSON(courseJson);
            	if (course != NULL) {
                	// 添加到链表头部或尾部，根据需要
                	course->next = head;
                	head = course;
            	}
            		courseJson = courseJson->next;
        	}
    	}
    	cJSON_Delete(root);
	mysql_free_result(result);
 	closeDatabase();
	return student1;
}



